
import java.io.IOException;
import java.util.logging.*;

public class crudHelper {

    private static final Logger crudLog = Logger.getLogger(crudHelper.class.getName());


    void crudFunc(){

        //Do some crud operation
        crudLog.log(Level.INFO ,"Create successful");
        //alternate way
        crudLog.info("Create successful 1");

        //logger's default handler is console
        ///default format in simple

        crudLog.setLevel(Level.WARNING);

        crudLog.info("unable to log since this is on lower level");

        crudLog.severe("Create successful2");

        // writing to console in xml format

        Handler handler = new ConsoleHandler();
        handler.setFormatter(new XMLFormatter());
        crudLog.addHandler(handler);
        crudLog.severe("logger info message");


    }

    void crudFuncFileHandler() throws IOException {


        Handler fileHandler = new FileHandler("logfile.txt");
        crudLog.addHandler(fileHandler);

        // do some crud crud functionality

        // by default file handler uses xml format
        crudLog.info("Read successful ");

        //changing to simple format
        fileHandler.setFormatter(new SimpleFormatter());

        crudLog.info("read successful using simple formatter ");
    }
}
